<?php
add_action('init','goto_login_page');
function goto_login_page() {
/* Syfte      : Fånga upp och vidarebefodra till en egen login sida
   Författare : Karin H Olsson karin@kobotolo.se 
   WP ver     : -
   URL        : http:/kobotolo.se
   Git rep    : https://bitbucket.org/kaolss/useful/src
*/

	$query = get_pages( array(
    	'meta_key'   => '_wp_page_template',
    	'meta_value' => 'login-page.php',
		) );
 
	if ( $query ) {
	    foreach ( $query as $post ) :
	        setup_postdata( $post ); 
	        $page_id=$post->ID;
      	endforeach;
	}
	$login_page = home_url( '/?page_id='. $page_id. '////' );
	$page = basename($_SERVER['REQUEST_URI']);

	if( $page == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
	   wp_redirect( $login_page  .'/');
	    	exit;
	}
}


add_action( 'wp_login_failed', 'login_failed' );
function login_failed() {
	$query = get_pages( array(
    	'meta_key'   => '_wp_page_template',
    	'meta_value' => 'login-page.php',
	));
	if ( $query ) {
	    foreach ( $query as $post ) :
	        setup_postdata( $post ); 
	        $page_id=$post->ID;
      	endforeach;
	}
	$login_page = home_url( '/?page_id='. $page_id. '/' );
	wp_redirect( $login_page . '&login=failed' );
	exit;
}

function blank_username_password( $user, $username, $password ) {
  
	$query = get_pages( array(
     	'meta_key'   => '_wp_page_template',
    	'meta_value' => 'login-page.php',
	));
	if ( $query ) {
	    foreach ( $query as $post ) :
	        setup_postdata( $post ); 
	        $page_id=$post->ID;
      	endforeach;
	}
	$login_page = home_url( '/?page_id='. $page_id. '/' );	
        if( $username == "" && $password == "" ) {
	   wp_redirect( $login_page  .'/');
	   exit;
	}
        if( $username == "" || $password == "" ) {
	   wp_redirect( $login_page . "&login=blank" );
	   exit;
	}
}

add_filter( 'authenticate', 'blank_username_password', 1, 3);
