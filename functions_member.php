<?php

add_filter( 'logout_redirect', 'kobotolo_my_logout_redirect', 10, 3 );
/* Syfte      : Omdirigera ej medlemmar till vald sida (hem)
   Författare : Karin H Olsson karin@kobotolo.se 
   WP ver     : -
   URL        : http:/kobotolo.se
   Git rep    : https://bitbucket.org/kaolss/useful/src
*/
function kobotolo_my_logout_redirect( $redirect_to, $request, $user ) {

   	//is there a user to check?
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		//check for admins
		if ( in_array( 'administrator', $user->roles ) ) {
			// redirect them to the default place
			return $redirect_to;
		} else {
			return home_url();
		}
	} else {
		return $redirect_to;
	}
}

add_filter( 'login_redirect', 'kobotolo_my_login_redirect', 10, 3 );
/* Syfte      : Omdirigera ej medlemmar till vald sida -- anpassad till svampkonsulenterna
   Författare : Karin H Olsson karin@kobotolo.se 
   WP ver     : -
   URL        : http:/kobotolo.se
   Git rep    : https://bitbucket.org/kaolss/useful/src
*/
function kobotolo_my_login_redirect( $redirect_to, $request, $user ) {

   	//is there a user to check?
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		//check for admins
		if ( in_array( 'administrator', $user->roles ) ) {
			// redirect them to the default place
			return $redirect_to;
		} else {
			return home_url().'/medlemssidor';
		}
	} else {
		return $redirect_to;
	}
}


add_action( 'admin_menu', 'kobotolo_stop_access_profile' );
/* Syfte      : Förhindra att medlemmar som delar profil byter passord för varandra 
	        De ska inte kunna ta sig hit - men för säkerhets skull
   Författare : Karin H Olsson karin@kobotolo.se 
   WP ver     : -
   URL        : http:/kobotolo.se
   Git rep    : https://bitbucket.org/kaolss/useful/src
*/
function kobotolo_stop_access_profile() {

    if ( ! current_user_can( 'manage_options' ) ) {
        remove_menu_page( 'profile.php' );
        remove_submenu_page( 'users.php', 'profile.php' );
        if('IS_PROFILE_PAGE' === true) {
            //Ett annat sätt att säga - drå åt fanders
            wp_die( 'Du kan inte ändra profilsidan. Kontakta en administratör om du vill ändra.' );
        }
    }
}


add_filter('show_admin_bar', 'kobotolo_hide_admin_bar_for_members');
/* Syfte      : Dölj adminbar för att inte skapa förvirring  
   Författare : Karin H Olsson karin@kobotolo.se 
   WP ver     : -
   URL        : http:/kobotolo.se
   Git rep    : https://bitbucket.org/kaolss/useful/src
*/
function kobotolo_hide_admin_bar_for_members() {

    if ( ! current_user_can( 'manage_options' ) ) {
        return false;
    }
    return true;
}

add_action('init','kobotolo_block_wp_admin',0);
/* Syfte      : Stoppar kreativa personer som trots allt försöker ta sig in i  wp-admin
   Författare : Karin H Olsson karin@kobotolo.se 
   WP ver     : -
   URL        : http:/kobotolo.se
   Git rep    : https://bitbucket.org/kaolss/useful/src
*/

function kobotolo_block_wp_admin() {
    if (strpos(strtolower($_SERVER['REQUEST_URI']),'/wp-admin/') !== false) {

        if ( ! current_user_can( 'manage_options' ) ) {
                wp_redirect( get_option('siteurl'), 302 );
        }
    }
}

add_filter('widget_text', 'do_shortcode');
add_shortcode( 'login_link' , 'kobotolo_login_link'); 
/* Syfte      : Shortcode login_link gör att man kan skapa login vat som helst
   Författare : Karin H Olsson karin@kobotolo.se 
   WP ver     : -
   URL        : http:/kobotolo.se
   Git rep    : https://bitbucket.org/kaolss/useful/src
*/
function kobotolo_login_link () {
    ob_start();
    $str='<div id="site-login">'.wp_loginout().'</div>';
    $content = ob_get_clean();
    return $content;
 }


add_action('init','goto_login_page');
function goto_login_page() {
/* Syfte      : Fånga upp och vidarebefodra till en egen login sida
   Författare : Karin H Olsson karin@kobotolo.se 
   WP ver     : -
   URL        : http:/kobotolo.se
   Git rep    : https://bitbucket.org/kaolss/useful/src
*/

	$query = get_pages( array(
    	'meta_key'   => '_wp_page_template',
    	'meta_value' => 'login-page.php',
		) );


 
	if ( $query ) {
	    foreach ( $query as $post ) :
	        setup_postdata( $post ); 
	        $page_id=$post->ID;
      	endforeach;
	}
	$login_page = home_url( '/?page_id='. $page_id. '////' );
	$page = basename($_SERVER['REQUEST_URI']);

	if( $page == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
	   wp_redirect( $login_page  .'/');
	    	exit;
	}
}


add_action( 'wp_login_failed', 'login_failed' );
function login_failed() {
	$query = get_pages( array(
    	'meta_key'   => '_wp_page_template',
    	'meta_value' => 'login-page.php',
	));
	if ( $query ) {
	    foreach ( $query as $post ) :
	        setup_postdata( $post ); 
	        $page_id=$post->ID;
      	endforeach;
	}
	$login_page = home_url( '/?page_id='. $page_id. '/' );
	wp_redirect( $login_page . '&login=failed' );
	exit;
}

function blank_username_password( $user, $username, $password ) {
  
	$query = get_pages( array(
     	'meta_key'   => '_wp_page_template',
    	'meta_value' => 'login-page.php',
	));
	if ( $query ) {
	    foreach ( $query as $post ) :
	        setup_postdata( $post ); 
	        $page_id=$post->ID;
      	endforeach;
	}
	$login_page = home_url( '/?page_id='. $page_id. '/' );	
    if( $username == "" && $password == "" ) {
	   wp_redirect( $login_page  .'/');
	   exit;
	}
    if( $username == "" || $password == "" ) {
	   wp_redirect( $login_page . "&login=blank" );
	   exit;
	}
}

add_filter( 'authenticate', 'blank_username_password', 1, 3);
