<?php
/*
Template Name: Login/Logout Page
*/

add_action( 'genesis_entry_content', 'ck_do_login_form' );
function ck_do_login_form() {

	$loggedin = is_user_logged_in();
	$user = wp_get_current_user();
	if ( $loggedin ) { ?>
		<h3>Du är redan inloggad</h3>
		<p>Hej, <?php echo $user->user_firstname; ?>! Du har redan loggat in!</p>
		<p>Gå <a href="/">Hem</a> eller <a href="<?php echo wp_logout_url( get_permalink() ); ?>">Logga ut</a></p>
	<?php
	} else {

		if (isset($_GET['login'])){
	    	echo 'Du har använt felaktiga inloggningsuppgifter.';
		}		
		$args = array(
			'form_id'			=> 'loginform',
			'redirect'			=> get_bloginfo( 'url' ),
			'id_username'		=> 'user_login',
			'id_password'		=> 'user_pass',
			'id_remember'		=> 'rememberme',
			'id_submit'			=> 'wp-submit',
			'label_username'	=> __( 'Username' ),
			'label_password'	=> __( 'Password' ),
			'label_remember'	=> __( 'Remember Me' ),
			'label_log_in'		=> __( 'Log In' ),
		);
		wp_login_form( $args );
	}
}
genesis();
